# Tutorial-PyMunk

[![Stacked Ball Drop](https://img.youtube.com/vi/2UHS883_P60/0.jpg)](https://www.youtube.com/watch?v=2UHS883_P60)


## Screenshot about examples
![Example.0](example.0.gif)

![Example.1](example.1.gif)

## Installs & Executes examples

### Download code

> https://gitlab.com/ioinfinity-play/tutorial-pymunk/-/archive/master/tutorial-pymunk-master.zip

### Use virtual enviroment for Python 

> source env/bin/activate


### Run example in your terminal

> pip install -r requirements.txt

> python youtube_bouncy_balls_0.py

> python youtube_bouncy_balls_1.py