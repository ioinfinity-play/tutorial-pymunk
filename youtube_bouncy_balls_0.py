"""This example spawns (bouncing) balls randomly on a L-shape constructed of 
two segment shapes. Not interactive.
"""

__version__ = "$Id:$"
__docformat__ = "reStructuredText"

# Python imports
import random
import sys

# Library imports
import pygame
from pygame.key import *
from pygame.locals import *
from pygame.color import *

# pymunk imports
import pymunk
import pymunk.pygame_util


class BouncyBalls(object):
    """
    Builds bouncy balls and view balls interactive each other.
    建立多個彈力球並觀察他們彼此互動狀態
    """

    def __init__(self):
        # Creates Space
        # 建立重力空間
        self._space = pymunk.Space()

        # Set gravity to Space
        # 設定空間重力加速度
        # https://zh.wikipedia.org/wiki/%E5%BC%95%E5%8A%9B
        # 影片說明Free Fall
        # https://www.youtube.com/watch?v=Sw2P1hpwpqU
        self._space.gravity = (0.0, -980.0)

        # pygame
        # Initializes pygame
        # 初始化PyGame
        pygame.init()
        # Set width and height of PyGame window.
        # 設定PyGame的寬跟高
        self._screen = pygame.display.set_mode((600, 800))

        # Used to manage how fast the screen updates
        # 管理PyGame更新畫面速度
        self._clock = pygame.time.Clock()

        # PyMunk draw Space into PyGame.
        # PyMunk 在PyGame中畫出重力空間
        self._draw_options = pymunk.pygame_util.DrawOptions(self._screen)

        # Balls that exist in the world
        # 建立球體籃子
        self._balls = []

        # Execution control and time until the next ball spawns
        # 控制程式是否繼續執行
        self._running = True
        # 控制下一個球體產生的時間
        self._ticks_to_next_ball = -1
        self._create_ball_frequently = 500
        # move simulation forward 0.01 seconds
        # Parameters:dt (float) – Time(1 second) step length
        # 播放動作快慢
        self._dt = 0.01


    def _add_static_line_scenery(self):
        """
        Create the static bodies.
        建立靜止物體
        :return: None
        """
        static_body = self._space.static_body
        # 建立兩點直線
        # 並給於線條粗細大小
        static_lines = [pymunk.Segment(
            static_body,
            (150, 15),
            (400, 15),
            5.0), ]
        for line in static_lines:
            # 彈力係數
            line.elasticity = 0.8
            # 摩擦係數
            line.friction = 0.9
        self._space.add(static_lines)

    def _create_fall_ball(self,
                          horizontal_pos,
                          vertical_pos,
                          mass,
                          radius,
                          elasticity,
                          friction):
        """
        Create a fall ball.
        :return:
        """
        # 慣性
        # Calculate the moment of inertia for a hollow circle
        # inner_radius and outer_radius are the inner and outer diameters.
        # (A solid circle has an inner diameter of 0)
        inertia = pymunk.moment_for_circle(mass, 0, radius, (0, 0))
        body = pymunk.Body(mass, inertia)
        # 球放置平行位置
        body.position = (horizontal_pos, vertical_pos)
        shape = pymunk.Circle(body, radius, (0, 0))
        # 彈力係數
        shape.elasticity = elasticity
        # 摩擦係數
        shape.friction = friction
        self._space.add(body, shape)
        self._balls.append(shape)

    def _creates_balls(self):
        # Ball attributes:
        # 1) horizontal_pos(平行位置)
        # 2) vertical_pos(垂直高度位置)
        # 3) mass(球體質量)
        # 4) radius(球體半徑)
        # 5) elasticity(球體彈性)
        # 6) friction(球體磨擦係數)

        # 建立沒有固定位置的球體
        '''
        balls = [
            (
                250+random.random()*100,
                390+random.random()*100,
                8,
                10,
                0.95,
                0.20
            )
        ]
        '''
        balls = [
            (150,
                390,
                8,
                10,
                0.95,
                0.20),
            (320,
                390,
                8,
                10,
                0.95,
                0.0),
            (320.0001,
                350,
                15,
                15,
                0.95,
                0.0),
            (320,
                300,
                25,
                25,
                0.95,
                0.0)
        ]
        
        for (hp, vp, mass, radius, elasticity, friction) in balls:
            self._create_fall_ball(
                hp,
                vp,
                mass,
                radius,
                elasticity,
                friction
            )

    def _process_events(self):
        """
        Handle game and events like keyboard input. Call once per frame only.
        :return: None
        """
        for event in pygame.event.get():
            if event.type == QUIT:
                self._running = False

    def _update_balls(self):
        """
        1) 新增/移除球體
        2) 每個Frame會執行一次
        Create/remove balls as necessary. Call once per frame only.
        :return: None
        """
        self._ticks_to_next_ball -= 1
        # 啟動產生球的機制
        if self._ticks_to_next_ball <= 0:
            # 產生多顆球體
            self._creates_balls()
            # 重新延長下次球體產生時間
            self._ticks_to_next_ball = self._create_ball_frequently
        # 移除垂直高度小於5的球
        # Remove balls that fall below 5 vertically
        balls_to_remove = [
            ball for ball in self._balls if ball.body.position.y < 5]
        for ball in balls_to_remove:
            self._space.remove(ball, ball.body)
            self._balls.remove(ball)

    def _reset_screen(self):
        """
        Clears the screen.
        :return: None
        """
        self._screen.fill(THECOLORS["white"])

    def _draw_objects(self):
        """
        Draw the objects.
        :return: None
        """
        self._space.debug_draw(self._draw_options)

    def run(self):
        """
        執行產生球體主程式
        """
        # 使用無限回圈產生連續性畫面
        # _running: 執行開關
        while self._running:
            self._space.step(self._dt)
            # 重新設定畫面
            self._reset_screen()
            # 更新球體畫面
            self._update_balls()
            # PyGame中畫出PyMunk2D物體
            self._draw_objects()
            # Pygame 翻動畫面
            pygame.display.flip()
            # Delay fixed time between frames
            # 給予每個Frame之間的一個延遲時間
            self._clock.tick(60)
            self._process_events()


if __name__ == '__main__':
    game = BouncyBalls()
    game.__init__()
    # Static barrier walls (lines) that the balls bounce off of
    # 新增靜止物體屏障. 例如: 線或是直線牆
    # 使得彈球可以依據重力進行反彈
    game._add_static_line_scenery()
    game.run()
